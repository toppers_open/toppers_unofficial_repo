# How to install

These are for kernel developers.
I run the example under OSX 10.6. It should runs under unices like Linux, BSD.
But I don't know how to run repo under win32 native.

## IMPORTANT NOTICE before install

Note that these repositories are UNOFFICIAL.
Please get source archives at toppers.jp if you want sure resources.
Don't ask at official mailing list about this repository.

Basically, no one helps any issues around this repository.


## Downloading repo script.

	$ mkdir -p ~/bin
	$ curl https://dl-ssl.google.com/dl/googlesource/git-repo/repo > ~/bin/repo
	$ chmod 755 ~/bin/repo

repo is implemented on the shell script and it uses python and git.
So you may have to install some additional commands.

repo is famous as the repository management tool for Android environment.
It may helpful to watch web resources describes about Android kernel.

You don't need to install repo if you have already installed.

And you should make sure to add ~/bin/repo in PATH environment.

### cloning TOPPERS/SSP kernel (unofficial)

At the first you should make working directory like this :

	$ rm -fr ssp-unofficial
	mkdir ssp-unofficial
	$ cd ssp-unofficial
	
And initialize.

	$ repo init -u https://bitbucket.org/monaka/toppers_unofficial_repo -b ssp

You'll get some verbose messages. Then you put this command.

	$ repo sync

You get sources in unofficial/ directory after some additional verbose messages.

#### note

There have no support around cfg. This should be fixed later.
